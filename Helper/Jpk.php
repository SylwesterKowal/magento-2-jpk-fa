<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Jpk extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context                              $context,
        \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory,
        \Kowal\WFirma\Service\CountriesTax                                 $countriesTax,
        \Kowal\JPK\Helper\Data                                             $dataHelper,
        \Magento\Tax\Api\TaxCalculationInterface                           $taxCalculation
    )
    {

        $this->invoiceCollectionFactory = $invoiceCollectionFactory;
        $this->countriesTax = $countriesTax;
        $this->dataHelper = $dataHelper;
        $this->taxCalculation = $taxCalculation;
        parent::__construct($context);
    }

    public function generateJpk($sklepy, $okres, $rok, $miesiac, $kwartal)
    {

        if ($okres == 'M') {
            $dates = $this->generateDateRangeFromMonth($miesiac, $rok);

            $startDate = $dates['firstDay'];
            $endDate = $dates['lastDay'];
        } else {
            $dates = $this->generateDateRangeFromQuarter($kwartal, $rok);

            $startDate = $dates['firstDay'];
            $endDate = $dates['lastDay'];
        }

        $filter = [
            'invoice_jpk' => ['null' => true, 'eq' => ''],
            'created_at' => ['from' => $startDate, 'to' => $endDate],
            'store_id' => ['in' => $sklepy]
        ];

        $store_id = $sklepy[0];

        $collection = $this->getInvoiceCollection($filter);


        if ($collection->getSize()) {

            // Definiowanie nagłówków dla pliku XML
            $xml = new \DOMDocument('1.0', 'UTF-8');


            $xml->formatOutput = true;
            // Utworzenie korzenia XML
            $jpk = $xml->createElement("JPK");
            $xml->appendChild($jpk);

            // Ustawienie atrybutów przestrzeni nazw (Namespace)
            $jpk->setAttribute('xmlns', 'http://jpk.mf.gov.pl/wzor/2022/02/17/02171/');
            $jpk->setAttribute('xmlns:etd', 'http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2018/08/24/eD/DefinicjeTypy/');
            $jpk->setAttribute('xmlns:tns', 'http://jpk.mf.gov.pl/wzor/2022/02/17/02171/');

            // Ustawienie strefy czasowej na UTC
            date_default_timezone_set('UTC');

            // Pobranie bieżącej daty i czasu w formacie ISO 8601
            $currentDateTimeISO8601 = date('Y-m-d\TH:i:s\Z');

            // Dodawanie przykładowego nagłówka
            $header = $xml->createElement("Naglowek");
            $jpk->appendChild($header);
            $kodFormularza = $xml->createElement("KodFormularza", "JPK_FA");
            $header->appendChild($kodFormularza);
            $kodFormularza->setAttribute('wersjaSchemy', '1-0');
            $kodFormularza->setAttribute('kodSystemowy', 'JPK_FA (4)');
            $header->appendChild($xml->createElement("WariantFormularza", "4"));
            $header->appendChild($xml->createElement("CelZlozenia", "1"));
            $header->appendChild($xml->createElement("DataWytworzeniaJPK", $currentDateTimeISO8601));
            $header->appendChild($xml->createElement("tns:DataOd", date('Y-m-d', strtotime($startDate))));
            $header->appendChild($xml->createElement("tns:DataDo", date('Y-m-d', strtotime($endDate))));
            $header->appendChild($xml->createElement("KodUrzedu", (string)$this->dataHelper->getKodUrzedu($store_id)));

            // Sekcja faktur
            $podmiot1 = $xml->createElement("Podmiot1");
            $jpk->appendChild($podmiot1);
            $IdentyfikatorPodmiotu = $xml->createElement("IdentyfikatorPodmiotu");
            $podmiot1->appendChild($IdentyfikatorPodmiotu);
            $ientyfikatorPodmiotu = $this->dataHelper->getIentyfikatorPodmiotu($store_id);
            if (is_null($ientyfikatorPodmiotu)) {
                throw new \Exception('Wypełnij ustawienia modułu JPK FA');
            }
            $IdentyfikatorPodmiotu->appendChild($xml->createElement("tns:NIP", $ientyfikatorPodmiotu));
            $IdentyfikatorPodmiotu->appendChild($xml->createElement("tns:PelnaNazwa", $this->dataHelper->getPelnaNazwa($store_id)));

            $adresPodmiotu = $xml->createElement("AdresPodmiotu");
            $podmiot1->appendChild($adresPodmiotu);
            $adresPodmiotu->appendChild($xml->createElement("etd:KodKraju", $this->dataHelper->getKodKraju($store_id)));
            $adresPodmiotu->appendChild($xml->createElement("etd:Wojewodztwo", $this->dataHelper->getWojewodztwo($store_id)));
            $adresPodmiotu->appendChild($xml->createElement("etd:Powiat", $this->dataHelper->getPowiat($store_id)));
            $adresPodmiotu->appendChild($xml->createElement("etd:Gmina", $this->dataHelper->getGmina($store_id)));
            $adresPodmiotu->appendChild($xml->createElement("etd:Ulica", $this->dataHelper->getAdresSprzedawcy($store_id)));
            $adresPodmiotu->appendChild($xml->createElement("etd:NrDomu", $this->dataHelper->getNumerDomu($store_id)));
            $adresPodmiotu->appendChild($xml->createElement("etd:NrLokalu", $this->dataHelper->getNumerLokalu($store_id)));
            $adresPodmiotu->appendChild($xml->createElement("etd:Miejscowosc", $this->dataHelper->getMiastoSprzedawcy($store_id)));
            $adresPodmiotu->appendChild($xml->createElement("etd:KodPocztowy", $this->dataHelper->getKodPocztowySprzedawcy($store_id)));

            $totalInvoicesValue = 0;
            $totalInvoicesCount = 0;
            foreach ($collection as $invoice) {

                $order = $invoice->getOrder();
                $billingAddress = $order->getBillingAddress();
                $nip = (string)$billingAddress->getVatId();
                if ($this->validateNip($nip)) {
//                    echo $invoice->getIncrementId() . PHP_EOL;

                    $faktura = $xml->createElement("Faktura");
                    $jpk->appendChild($faktura);

                    $totalInvoicesValue += (float)$order->getTotalInvoiced();
                    $totalInvoicesCount++;
                    $faktura->appendChild($xml->createElement("KodWaluty", $invoice->getOrderCurrencyCode()));
                    $faktura->appendChild($xml->createElement("P_1", date('Y-m-d', strtotime($invoice->getCreatedAt()))));
                    $faktura->appendChild($xml->createElement("P_2A", $invoice->getIncrementId()));

                    $company_name = str_replace("&", " and ", (string)$billingAddress->getCompany());
                    $company_name = $company_name ?
                        (string)$company_name :
                        (string)$billingAddress->getFirstname() . ' ' . (string)$billingAddress->getLastname();

                    $faktura->appendChild($xml->createElement("P_3A", $company_name));
                    $faktura->appendChild($xml->createElement("P_3B", (string)$billingAddress->getPostcode() . ' ' . (string)$billingAddress->getCity() . ', ' . (string)$billingAddress->getStreetLine(1) . ', ' . $billingAddress->getCountryId()));
                    $faktura->appendChild($xml->createElement("P_3C", (string)$this->dataHelper->getPelnaNazwa($order->getStoreId())));
                    $faktura->appendChild($xml->createElement("P_3D", $this->dataHelper->getKodPocztowySprzedawcy($order->getStoreId()) . ' ' . $this->dataHelper->getMiastoSprzedawcy($order->getStoreId()) . ' ' . $this->dataHelper->getAdresSprzedawcy($order->getStoreId()) . ' ' . $this->dataHelper->getNumerDomu($order->getStoreId()) . ' ' . $this->dataHelper->getNumerLokalu($order->getStoreId())));
                    $faktura->appendChild($xml->createElement("P_4B", (string)$this->dataHelper->getIentyfikatorPodmiotu($store_id)));
                    $faktura->appendChild($xml->createElement("P_5B", (string)$billingAddress->getVatId()));
                    $faktura->appendChild($xml->createElement("P_6", date('Y-m-d', strtotime($invoice->getCreatedAt()))));
                    $faktura->appendChild($xml->createElement("P_13_6", number_format((float)$order->getTotalInvoiced(), 0, ".", "")));
                    $faktura->appendChild($xml->createElement("P_15", number_format((float)$order->getTotalInvoiced(), 2, ".", "")));
                    $faktura->appendChild($xml->createElement("P_16", "false"));
                    $faktura->appendChild($xml->createElement("P_17", "false"));
                    $faktura->appendChild($xml->createElement("P_18", "false"));
                    $faktura->appendChild($xml->createElement("P_18A", "false"));
                    $faktura->appendChild($xml->createElement("P_19", "false"));
                    $faktura->appendChild($xml->createElement("P_20", "false"));
                    $faktura->appendChild($xml->createElement("P_21", "false"));
                    $faktura->appendChild($xml->createElement("P_22", "false"));
                    $faktura->appendChild($xml->createElement("P_23", "false"));
                    $faktura->appendChild($xml->createElement("P_106E_2", "false"));
                    $faktura->appendChild($xml->createElement("P_106E_3", "false"));
                    $faktura->appendChild($xml->createElement("RodzajFaktury", "VAT"));

                }
            }

            $fakturaCtrl = $xml->createElement("FakturaCtrl");
            $jpk->appendChild($fakturaCtrl);
            $fakturaCtrl->appendChild($xml->createElement("LiczbaFaktur", (string)$totalInvoicesCount));
            $fakturaCtrl->appendChild($xml->createElement("WartoscFaktur", (string)$totalInvoicesValue));

            $liczbaWierszyFaktur = 0;
            $wartoscWierszyFaktur = 0;
            foreach ($collection as $invoice) {

                $order = $invoice->getOrder();
                $billingAddress = $order->getBillingAddress();
                $nip = (string)$billingAddress->getVatId();
                if ($this->validateNip($nip)) {
//                    echo $invoice->getIncrementId() . PHP_EOL;

                    foreach ($order->getAllItems() as $item) {
                        if ($item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                            continue;
                        }
                        if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                            continue;
                        }

                        $liczbaWierszyFaktur++;
                        $fakturaWiersz = $xml->createElement("FakturaWiersz");
                        $jpk->appendChild($fakturaWiersz);

                        $priceBrutto_ = $item->getPrice() - ($item->getDiscountAmount() / $item->getQtyOrdered());
                        $priceBrutto = number_format((float)$priceBrutto_, 2, ".", "");
                        $rowTotal_ = $item->getRowTotal() - $item->getDiscountAmount();
                        $rowTotal = number_format((float)$rowTotal_, 2, ".", "");
                        $fakturaWiersz->appendChild($xml->createElement("P_2B", $invoice->getIncrementId()));
                        $fakturaWiersz->appendChild($xml->createElement("P_7", $item->getSku()));
                        $fakturaWiersz->appendChild($xml->createElement("P_8A", "szt."));
                        $fakturaWiersz->appendChild($xml->createElement("P_8B", (string)$item->getQtyOrdered()));
                        $fakturaWiersz->appendChild($xml->createElement("P_9B", number_format((float)$priceBrutto, 2, ".", "")));
                        $wartoscWierszyFaktur += $rowTotal;
                        $fakturaWiersz->appendChild($xml->createElement("P_11A", number_format((float)$rowTotal, 2, ".", "")));

                        $countryId = is_null($order->getShippingAddress()) ? "" : $order->getShippingAddress()->getCountryId();
                        $tax = (int)$this->countriesTax->getTaxRate('standard', $countryId);

                        $fakturaWiersz->appendChild($xml->createElement("P_12", (string)$tax));
                    }


                    // DELIVERY
                    $delivery_price = $order->getShippingAmount();
                    $delivery_price_brutto = $order->getShippingInclTax();
                    $delivery_tax_amount =  $order->getShippingTaxAmount();

                    $taxClassId = $this->dataHelper->getShippingTaxClass($store_id);
                    $delivery_tax_percent = $this->getTaxPercent($taxClassId, $order);

                    if($delivery_price > 0){
                        $liczbaWierszyFaktur++;
                        $fakturaWiersz = $xml->createElement("FakturaWiersz");
                        $jpk->appendChild($fakturaWiersz);

                        $fakturaWiersz->appendChild($xml->createElement("P_2B", $invoice->getIncrementId()));
                        $fakturaWiersz->appendChild($xml->createElement("P_7", "DOSTAWA"));
                        $fakturaWiersz->appendChild($xml->createElement("P_8A", "szt."));
                        $fakturaWiersz->appendChild($xml->createElement("P_8B", "1"));
                        $fakturaWiersz->appendChild($xml->createElement("P_9B", number_format((float)$delivery_price_brutto, 2, ".", "")));
                        $wartoscWierszyFaktur += $delivery_price_brutto;
                        $fakturaWiersz->appendChild($xml->createElement("P_11A", number_format((float)$delivery_price_brutto, 2, ".", "")));

                        $countryId = is_null($order->getShippingAddress()) ? "" : $order->getShippingAddress()->getCountryId();
                        $tax = (int)$this->countriesTax->getTaxRate('standard', $countryId);

                        $fakturaWiersz->appendChild($xml->createElement("P_12", (string)$tax));

                    }

                }
            }

            $fakturaWierszCtrl = $xml->createElement("FakturaWierszCtrl");
            $jpk->appendChild($fakturaWierszCtrl);
            $fakturaWierszCtrl->appendChild($xml->createElement("LiczbaWierszyFaktur", (string)$liczbaWierszyFaktur));
            $fakturaWierszCtrl->appendChild($xml->createElement("WartoscWierszyFaktur", (string)round($wartoscWierszyFaktur, 2)));

            // Zapisywanie XML do bazy
            return ['content' => $xml->saveXML(), 'dataOd' => $startDate, 'dataDo' => $endDate, 'sklepy' => $this->dataHelper->getPelnaNazwa($store_id)];
        }
    }

    private function getTaxPercent($taxClassId, $order)
    {
        $tax = $this->taxCalculation->getCalculatedRate(
            $taxClassId,
            $order->getCustomerId(),
            $order->getStoreId()
        );

        return $this->formatNumber($tax);
    }

    private function formatNumber($amount)
    {
        return number_format((float)$amount, 2, '.', '');
    }

    public function getInvoiceCollection(array $filters = [])
    {

        $collection = $this->invoiceCollectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }

        return $collection;
    }

    public function validateNip($vatid)
    {
        $nipWithoutDashes = preg_replace("/-/", "", $vatid);
        $reg = '/^[0-9]{10}$/';
        if (preg_match($reg, $nipWithoutDashes) == false)
            return false;
        else {
            $digits = str_split($nipWithoutDashes);
            $checksum = (6 * intval($digits[0]) + 5 * intval($digits[1]) + 7 * intval($digits[2]) + 2 * intval($digits[3]) + 3 * intval($digits[4]) + 4 * intval($digits[5]) + 5 * intval($digits[6]) + 6 * intval($digits[7]) + 7 * intval($digits[8])) % 11;

            return (intval($digits[9]) == $checksum);
        }
    }

    function generateDateRangeFromMonth($monthNumber, $year = null)
    {
        // Ustawienie roku jako bieżący, jeśli nie podano inaczej
        $year = $year ?: date("Y");

        // Utworzenie daty pierwszego dnia miesiąca
        $firstDayOfMonth = "{$year}-{$monthNumber}-01";

        // Obliczenie ostatniego dnia miesiąca
        $lastDayOfMonth = date("Y-m-t", strtotime($firstDayOfMonth));

        return [
            'firstDay' => $firstDayOfMonth,
            'lastDay' => $lastDayOfMonth
        ];
    }

    function generateDateRangeFromQuarter($quarterNumber, $year = null)
    {
        // Ustawienie roku jako bieżący, jeśli nie podano inaczej
        $year = $year ?: date("Y");

        // Mapowanie numeru kwartału na miesiące
        $quarterMonths = [
            1 => ['start' => 1, 'end' => 3],  // Styczeń - Marzec
            2 => ['start' => 4, 'end' => 6],  // Kwieceń - Czerweiec
            3 => ['start' => 7, 'end' => 9],  // Lipiec - Wrzesień
            4 => ['start' => 10, 'end' => 12] // Paź - Grudzien
        ];

        // Obliczanie pierwszego i ostatniego miesiąca w kwartale
        $startMonth = $quarterMonths[$quarterNumber]['start'];
        $endMonth = $quarterMonths[$quarterNumber]['end'];

        // Utworzenie daty pierwszego dnia kwartału
        $firstDayOfQuarter = "{$year}-{$startMonth}-01";

        // Utworzenie daty ostatniego dnia kwartału
        $lastDayOfQuarter = date("Y-m-t", strtotime("{$year}-{$endMonth}-01"));

        return [
            'firstDay' => $firstDayOfQuarter,
            'lastDay' => $lastDayOfQuarter
        ];
    }
}