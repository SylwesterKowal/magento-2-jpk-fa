<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Model;

use Kowal\JPK\Api\Data\JpkInterface;
use Kowal\JPK\Api\Data\JpkInterfaceFactory;
use Kowal\JPK\Api\Data\JpkSearchResultsInterfaceFactory;
use Kowal\JPK\Api\JpkRepositoryInterface;
use Kowal\JPK\Model\ResourceModel\Jpk as ResourceJpk;
use Kowal\JPK\Model\ResourceModel\Jpk\CollectionFactory as JpkCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class JpkRepository implements JpkRepositoryInterface
{

    /**
     * @var Jpk
     */
    protected $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var ResourceJpk
     */
    protected $resource;

    /**
     * @var JpkInterfaceFactory
     */
    protected $jpkFactory;

    /**
     * @var JpkCollectionFactory
     */
    protected $jpkCollectionFactory;


    /**
     * @param ResourceJpk $resource
     * @param JpkInterfaceFactory $jpkFactory
     * @param JpkCollectionFactory $jpkCollectionFactory
     * @param JpkSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceJpk $resource,
        JpkInterfaceFactory $jpkFactory,
        JpkCollectionFactory $jpkCollectionFactory,
        JpkSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->jpkFactory = $jpkFactory;
        $this->jpkCollectionFactory = $jpkCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(JpkInterface $jpk)
    {
        try {
            $this->resource->save($jpk);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the jpk: %1',
                $exception->getMessage()
            ));
        }
        return $jpk;
    }

    /**
     * @inheritDoc
     */
    public function get($jpkId)
    {
        $jpk = $this->jpkFactory->create();
        $this->resource->load($jpk, $jpkId);
        if (!$jpk->getId()) {
            throw new NoSuchEntityException(__('Jpk with id "%1" does not exist.', $jpkId));
        }
        return $jpk;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->jpkCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(JpkInterface $jpk)
    {
        try {
            $jpkModel = $this->jpkFactory->create();
            $this->resource->load($jpkModel, $jpk->getJpkId());
            $this->resource->delete($jpkModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Jpk: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($jpkId)
    {
        return $this->delete($this->get($jpkId));
    }
}

