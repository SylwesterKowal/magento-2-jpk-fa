<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Api\Data;

interface JpkSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Jpk list.
     * @return \Kowal\JPK\Api\Data\JpkInterface[]
     */
    public function getItems();

    /**
     * Set okres list.
     * @param \Kowal\JPK\Api\Data\JpkInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

