<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Model;

use Kowal\JPK\Api\Data\JpkInterface;
use Magento\Framework\Model\AbstractModel;

class Jpk extends AbstractModel implements JpkInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Kowal\JPK\Model\ResourceModel\Jpk::class);
    }

    /**
     * @inheritDoc
     */
    public function getJpkId()
    {
        return $this->getData(self::JPK_ID);
    }

    /**
     * @inheritDoc
     */
    public function setJpkId($jpkId)
    {
        return $this->setData(self::JPK_ID, $jpkId);
    }

    /**
     * @inheritDoc
     */
    public function getOkres()
    {
        return $this->getData(self::OKRES);
    }

    /**
     * @inheritDoc
     */
    public function setOkres($okres)
    {
        return $this->setData(self::OKRES, $okres);
    }

    /**
     * @inheritDoc
     */
    public function getMiesiac()
    {
        return $this->getData(self::MIESIAC);
    }

    /**
     * @inheritDoc
     */
    public function setMiesiac($miesiac)
    {
        return $this->setData(self::MIESIAC, $miesiac);
    }

    /**
     * @inheritDoc
     */
    public function getKwartal()
    {
        return $this->getData(self::KWARTAL);
    }

    /**
     * @inheritDoc
     */
    public function setKwartal($kwartal)
    {
        return $this->setData(self::KWARTAL, $kwartal);
    }

    /**
     * @inheritDoc
     */
    public function getRok()
    {
        return $this->getData(self::ROK);
    }

    /**
     * @inheritDoc
     */
    public function setRok($rok)
    {
        return $this->setData(self::ROK, $rok);
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->getData(self::USERNAME);
    }

    /**
     * @inheritDoc
     */
    public function setUsername($username)
    {
        return $this->setData(self::USERNAME, $username);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @inheritDoc
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * @inheritDoc
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * @inheritDoc
     */
    public function getDataOd()
    {
        return $this->getData(self::DATAOD);
    }

    /**
     * @inheritDoc
     */
    public function setDataOd($dataOd)
    {
        return $this->setData(self::DATAOD, $dataOd);
    }

    /**
     * @inheritDoc
     */
    public function getDataDo()
    {
        return $this->getData(self::DATADO);
    }

    /**
     * @inheritDoc
     */
    public function setDataDo($dataDo)
    {
        return $this->setData(self::DATADO, $dataDo);
    }

    /**
     * @inheritDoc
     */
    public function getSklepy()
    {
        return $this->getData(self::SKLEPY);
    }

    /**
     * @inheritDoc
     */
    public function setSklepy($sklepy)
    {
        return $this->setData(self::SKLEPY, $sklepy);
    }
}

