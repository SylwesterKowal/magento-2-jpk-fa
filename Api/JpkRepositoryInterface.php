<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface JpkRepositoryInterface
{

    /**
     * Save Jpk
     * @param \Kowal\JPK\Api\Data\JpkInterface $jpk
     * @return \Kowal\JPK\Api\Data\JpkInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Kowal\JPK\Api\Data\JpkInterface $jpk);

    /**
     * Retrieve Jpk
     * @param string $jpkId
     * @return \Kowal\JPK\Api\Data\JpkInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($jpkId);

    /**
     * Retrieve Jpk matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\JPK\Api\Data\JpkSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Jpk
     * @param \Kowal\JPK\Api\Data\JpkInterface $jpk
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Kowal\JPK\Api\Data\JpkInterface $jpk);

    /**
     * Delete Jpk by ID
     * @param string $jpkId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($jpkId);
}

