<?php
namespace Kowal\JPK\Plugin;

class MessagePlugin
{
    public function beforeAddSuccessMessage(
        \Magento\Framework\Message\ManagerInterface $subject,
                                                    $message
    ) {
        // Zakładamy, że komunikat przychodzi w postaci HTML
        if (strpos($message, 'http') !== false) {
            return [new \Magento\Framework\Phrase($message)];
        }
        return null;
    }
}