<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Model\Config\Source;

class Rok implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        $currentYear = date("Y");
        $previousYear = $currentYear - 1;
        return [['value' => $previousYear, 'label' => (string)$previousYear],['value' => $currentYear, 'label' =>  (string)$currentYear]];
    }

    public function toArray()
    {
        $currentYear = date("Y");
        $previousYear = $currentYear - 1;
        return [$previousYear =>  (string)$previousYear,$currentYear =>  (string)$currentYear];
    }
}

