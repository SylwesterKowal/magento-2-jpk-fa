<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Api\Data;

interface JpkInterface
{

    const OKRES = 'okres';
    const USERNAME = 'username';
    const KWARTAL = 'kwartal';
    const MIESIAC = 'miesiac';
    const JPK_ID = 'jpk_id';
    const CONTENT = 'content';
    const CREATED_AT = 'created_at';
    const ROK = 'rok';
    const DATAOD = 'dataOd';
    const DATADO = 'dataDo';
    const SKLEPY = 'sklepy';

    /**
     * Get jpk_id
     * @return string|null
     */
    public function getJpkId();

    /**
     * Set jpk_id
     * @param string $jpkId
     * @return \Kowal\JPK\Jpk\Api\Data\JpkInterface
     */
    public function setJpkId($jpkId);

    /**
     * Get okres
     * @return string|null
     */
    public function getOkres();

    /**
     * Set okres
     * @param string $okres
     * @return \Kowal\JPK\Jpk\Api\Data\JpkInterface
     */
    public function setOkres($okres);

    /**
     * Get miesiac
     * @return string|null
     */
    public function getMiesiac();

    /**
     * Set miesiac
     * @param string $miesiac
     * @return \Kowal\JPK\Jpk\Api\Data\JpkInterface
     */
    public function setMiesiac($miesiac);

    /**
     * Get kwartal
     * @return string|null
     */
    public function getKwartal();

    /**
     * Set kwartal
     * @param string $kwartal
     * @return \Kowal\JPK\Jpk\Api\Data\JpkInterface
     */
    public function setKwartal($kwartal);

    /**
     * Get rok
     * @return string|null
     */
    public function getRok();

    /**
     * Set rok
     * @param string $rok
     * @return \Kowal\JPK\Jpk\Api\Data\JpkInterface
     */
    public function setRok($rok);

    /**
     * Get username
     * @return string|null
     */
    public function getUsername();

    /**
     * Set username
     * @param string $username
     * @return \Kowal\JPK\Jpk\Api\Data\JpkInterface
     */
    public function setUsername($username);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Kowal\JPK\Jpk\Api\Data\JpkInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get content
     * @return string|null
     */
    public function getContent();

    /**
     * Set content
     * @param string $content
     * @return \Kowal\JPK\Jpk\Api\Data\JpkInterface
     */
    public function setContent($content);

    /**
     * Get dataOd
     * @return string|null
     */
    public function getDataOd();

    /**
     * Set dataOd
     * @param string $dataOd
     * @return \Kowal\JPK\Jpk\Api\Data\JpkInterface
     */
    public function setDataOd($dataOd);

    /**
     * Get dataDo
     * @return string|null
     */
    public function getDataDo();

    /**
     * Set dataDo
     * @param string $dataDo
     * @return \Kowal\JPK\Jpk\Api\Data\JpkInterface
     */
    public function setDataDo($dataDo);

    /**
     * Get sklepy
     * @return string|null
     */
    public function getSklepy();

    /**
     * Set sklepy
     * @param string $sklepy
     * @return \Kowal\JPK\Jpk\Api\Data\JpkInterface
     */
    public function setSklepy($sklepy);
}

