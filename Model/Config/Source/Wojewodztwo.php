<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Model\Config\Source;

class Wojewodztwo implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'Dolnośląskie', 'label' => __('Dolnośląskie')], ['value' => 'Kujawsko-Pomorskie', 'label' => __('Kujawsko-Pomorskie')], ['value' => 'Lubelskie', 'label' => __('Lubelskie')], ['value' => 'Lubuskie', 'label' => __('Lubuskie')], ['value' => 'Łódzkie', 'label' => __('Łódzkie')], ['value' => 'Małopolskie', 'label' => __('Małopolskie')], ['value' => 'Mazowieckie', 'label' => __('Mazowieckie')], ['value' => 'Opolskie', 'label' => __('Opolskie')], ['value' => 'Podkarpackie', 'label' => __('Podkarpackie')], ['value' => 'Podlaskie', 'label' => __('Podlaskie')], ['value' => 'Pomorskie', 'label' => __('Pomorskie')], ['value' => 'Śląskie', 'label' => __('Śląskie')], ['value' => 'Świętokrzyskie', 'label' => __('Świętokrzyskie')], ['value' => 'Warmińsko-Mazurskie', 'label' => __('Warmińsko-Mazurskie')], ['value' => 'Wielkopolskie', 'label' => __('Wielkopolskie')], ['value' => 'Zachodniopomorskie', 'label' => __('Zachodniopomorskie')]];
    }

    public function toArray()
    {
        return ['Dolnośląskie' => __('Dolnośląskie'), 'Kujawsko-Pomorskie' => __('Kujawsko-Pomorskie'), 'Lubelskie' => __('Lubelskie'), 'Lubuskie' => __('Lubuskie'), 'Łódzkie' => __('Łódzkie'), 'Małopolskie' => __('Małopolskie'), 'Mazowieckie' => __('Mazowieckie'), 'Opolskie' => __('Opolskie'), 'Podkarpackie' => __('Podkarpackie'), 'Podlaskie' => __('Podlaskie'), 'Pomorskie' => __('Pomorskie'), 'Śląskie' => __('Śląskie'), 'Świętokrzyskie' => __('Świętokrzyskie'), 'Warmińsko-Mazurskie' => __('Warmińsko-Mazurskie'), 'Wielkopolskie' => __('Wielkopolskie'), 'Zachodniopomorskie' => __('Zachodniopomorskie')];
    }
}