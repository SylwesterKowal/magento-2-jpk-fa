<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Jpk extends AbstractDb
{

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init('kowal_jpk_jpk', 'jpk_id');
    }
}

