<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Controller\Adminhtml\Jpk;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context                   $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Kowal\JPK\Helper\Jpk                                 $jpkHelper
    )
    {
        $this->dataPersistor = $dataPersistor;
        $this->jpkHelper = $jpkHelper;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if($data['okres'] == 'M'){
            $data['kwartal'] = null;
        }else{
            $data['miesiac'] = null;
        }

        $daneJPK = $this->jpkHelper->generateJpk($data['sklepy'], $data['okres'], $data['rok'], $data['miesiac'], $data['kwartal']);
        $data['content'] = (isset($daneJPK['content'])) ? $daneJPK['content'] : '' ;
        $data['dataOd'] = (isset($daneJPK['dataOd'])) ? $daneJPK['dataOd'] : '' ;
        $data['dataDo'] = (isset($daneJPK['dataDo'])) ? $daneJPK['dataDo'] : '' ;
        $data['sklepy'] = (isset($daneJPK['sklepy'])) ? $daneJPK['sklepy'] : '' ;
        if ($data) {
            $id = $this->getRequest()->getParam('jpk_id');

            $model = $this->_objectManager->create(\Kowal\JPK\Model\Jpk::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Jpk no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the JPK FA.'));

                $this->dataPersistor->clear('kowal_jpk_jpk');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['jpk_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Jpk.'));
            }

            $this->dataPersistor->set('kowal_jpk_jpk', $data);
            return $resultRedirect->setPath('*/*/edit', ['jpk_id' => $this->getRequest()->getParam('jpk_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}

