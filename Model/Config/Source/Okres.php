<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Model\Config\Source;

class Okres implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'M', 'label' => __('M')],['value' => 'K', 'label' => __('K')]];
    }

    public function toArray()
    {
        return ['M' => __('M'),'K' => __('K')];
    }
}

