<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Model\Config\Source;

class Kwartal implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => '1', 'label' => __('1')],['value' => '2', 'label' => __('2')],['value' => '3', 'label' => __('3')],['value' => '4', 'label' => __('4')]];
    }

    public function toArray()
    {
        return ['1' => __('1'),'2' => __('2'),'3' => __('3'),'4' => __('4')];
    }
}

