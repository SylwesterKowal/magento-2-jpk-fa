<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context              $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Config\Model\ResourceModel\Config         $resourceConfig,
        \Magento\Store\Model\StoreManagerInterface         $storeManager,
        \Magento\Framework\App\RequestInterface            $request
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->resourceConfig = $resourceConfig;
        $this->storeManager = $storeManager;
        $this->request = $request;
        parent::__construct($context);
    }


    public function getIentyfikatorPodmiotu($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/identyfikator_podmiotu', $storeId);
    }

    public function getPelnaNazwa($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/pelna_nazwa', $storeId);
    }

    public function getKodUrzedu($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/kod_urzedu', $storeId);
    }

    public function getWojewodztwo($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/wojewodztwo', $storeId);
    }

    public function getPowiat($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/powiat', $storeId);
    }

    public function getGmina($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/gmina', $storeId);
    }

    public function getKodKraju($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/kod_kraju', $storeId);
    }

    public function getAdresSprzedawcy($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/adres_sprzedawcy', $storeId);
    }

    public function getNumerDomu($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/numer_domu', $storeId);
    }

    public function getNumerLokalu($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/numer_lokalu', $storeId);
    }


    public function getKodPocztowySprzedawcy($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/kod_pocztowy_sprzedawcy', $storeId);
    }

    public function getMiastoSprzedawcy($storeId = 0)
    {
        return $this->getConfigValue('jpk/parametry/miasto_sprzedawcy', $storeId);
    }

    public function getShippingTaxClass($storeId = 0)
    {
        return $this->getConfigValue('tax/classes/shipping_tax_class', $storeId);
    }

    public function getConfigValue($field, $storeId = null)
    {
        if ($storeIdRequest = (int)$this->request->getParam('store', 0)) {
            $storeId = $storeIdRequest;
        } else if (is_null($storeId)) {
            $storeId = $this->storeManager->getStore()->getId();
        }

        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

}