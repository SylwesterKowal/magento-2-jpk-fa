<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\JPK\Controller\Adminhtml\Jpk;

class Edit extends \Kowal\JPK\Controller\Adminhtml\Jpk
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context              $context,
        \Magento\Framework\Registry                      $coreRegistry,
        \Magento\Framework\View\Result\PageFactory       $resultPageFactory,
        \Magento\Framework\Controller\Result\RawFactory  $resultRawFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->fileFactory = $fileFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('jpk_id');
        $model = $this->_objectManager->create(\Kowal\JPK\Model\Jpk::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Jpk no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }else{
                $this->fileFactory->create(
                    'JPK_FA_' .  $model->getOkres().'_'.$model->getDataOd().'_'.$model->getDataDo().'_'.$id . '.xml',
                    $model->getContent(),
                    \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR
                );
                $resultRaw = $this->resultRawFactory->create();
                return $resultRaw;
            }
        }
        $this->_coreRegistry->register('kowal_jpk_jpk', $model);


        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Jpk') : __('Nowy JPK FA'),
            $id ? __('Edit Jpk') : __('Nowy JPK FA')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Jpks'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Jpk %1', $model->getId()) : __('Nowy JPK FA v4'));
        return $resultPage;
    }
}

